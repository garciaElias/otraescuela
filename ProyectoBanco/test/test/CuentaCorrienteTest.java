package test;

import static org.junit.Assert.*;

import org.junit.Test;

import banco.cuentas.CuentaCorriente;
import banco.cuentas.SaldoInsuficienteException;

public class CuentaCorrienteTest {

	@Test
	public void pruebaExtraccionMenorASaldoEnRojo() throws SaldoInsuficienteException {
		CuentaCorriente objetoCC = new CuentaCorriente("Elias", 36, 1, 10000, 1000);
		objetoCC.extraer(10500);
		assertEquals(-500, objetoCC.verSaldo(), 0.0);
	}

	@Test
	public void pruebaVerSaldoEnRojoCuandoNoEsUtilizado() throws SaldoInsuficienteException {
		CuentaCorriente objetoCC = new CuentaCorriente("Elias", 36, 1, 10000, 1000);
		objetoCC.extraer(9500);
		assertEquals(0, objetoCC.verSaldoEnRojo(), 0.0);
	}

	@Test(expected = SaldoInsuficienteException.class)
	public void pruebaVerSaldoConExtraccionMayorASaldoEnRojo() throws SaldoInsuficienteException {
		CuentaCorriente objetoCC = new CuentaCorriente("Elias", 36, 1, 10000, 1000);
		objetoCC.extraer(11500);
		assertEquals(10000, objetoCC.verSaldo(), 0.0);
	}

	@Test
	public void pruebaVerSaldoEnRojoCuandoEsUtilizado() throws SaldoInsuficienteException {
		CuentaCorriente objetoCC = new CuentaCorriente("Elias", 36, 1, 10000, 1000);
		objetoCC.extraer(10500);
		assertEquals(500, objetoCC.verSaldoEnRojo(), 0.0);
	}

	@Test
	public void pruebaToString() {
		CuentaCorriente objetoCuentaCorriente = new CuentaCorriente("Elias", 36, 1, 10000, 1000);
		String esperado = "Nombre: Elias Dni: 36 Numero de cuenta: 1 Saldo: 10000 Saldo en rojo: 1000";
		String actual = objetoCuentaCorriente.toString();
		assertEquals(esperado, actual);

	}

	@Test
	public void pruebaEqual() {
		CuentaCorriente objetoCuentaCorrienteUno = new CuentaCorriente("Elias", 36, 1, 10000, 1000);
		CuentaCorriente objetoCuentaCorrienteDos = new CuentaCorriente("Elias", 36, 1, 10000, 1000);

		assertEquals(true, objetoCuentaCorrienteUno.equals(objetoCuentaCorrienteDos));

	}

}
