package test;

import static org.junit.Assert.*;

import org.junit.Test;

import banco.cuentas.Cuenta;
import banco.cuentas.CuentaSueldo;
import banco.cuentas.SaldoInsuficienteException;

public class CuentaSueldoTest {

	@Test // no hace falta porque no va a saltar la exception (expected =
			// saldoInsuficienteException.class)
	public void pruebaVerSaldoExtrayendoMenosQueElTotal() throws SaldoInsuficienteException {

		CuentaSueldo objetoCuentaSueldo = new CuentaSueldo("Elias", 36, 1, 10000, "Epidata");
		objetoCuentaSueldo.extraer(9500);
		assertEquals(500, objetoCuentaSueldo.verSaldo(), 0.0);
	}

	@Test(expected = SaldoInsuficienteException.class) // en este caso SII va a
														// saltar la exception
	public void pruebaVerSaldoExtrayendoMasQueElTotal() throws SaldoInsuficienteException {

		CuentaSueldo objetoCuentaSueldo = new CuentaSueldo("Elias", 36, 1, 10000, "Epidata");
		objetoCuentaSueldo.extraer(10001);
		assertEquals(10000, objetoCuentaSueldo.verSaldo(), 0.0);
	}

	@Test
	public void pruebaToString() {
		CuentaSueldo objetoCuentaSueldo = new CuentaSueldo("Elias", 36, 1, 10000, "Epidata");
		String esperado = "Nombre: Elias Dni: 36 Numero de cuenta: 1 Saldo: 10000 Empleador: Epidata";
		String actual = objetoCuentaSueldo.toString();
		assertEquals(esperado, actual);

	}

	@Test
	public void pruebaEqual() {
		Cuenta objetoCuentaSueldoUno = new CuentaSueldo("Elias", 36, 1, 10000, "Epidata");
		Cuenta objetoCuentaSueldoDos = new CuentaSueldo("Elias", 36, 1, 10000, "Epidata");

		assertEquals(true, objetoCuentaSueldoUno.equals(objetoCuentaSueldoDos));

	}

}