package test;

import static org.junit.Assert.*;

import org.junit.Test;

import banco.cuentas.Cuenta;
import banco.cuentas.SaldoInsuficienteException;

public class CuentaTest {

	@Test
	public void pruebaVerSaldo() {
		Cuenta objetoCuenta = new Cuenta("Elias", 36, 1, 10000);
		assertEquals(10000, objetoCuenta.verSaldo(), 0.0);
	}

	@Test
	public void pruebaVerSaldoDespuesDeExtraccion() throws SaldoInsuficienteException {
		Cuenta objetoCuenta = new Cuenta("Elias", 36, 1, 10000);
		objetoCuenta.extraer(5000);
		assertEquals(5000, objetoCuenta.verSaldo(), 0.0);
	}

	@Test
	public void pruebaDepositarConValorEsperadoCorrecto() {
		Cuenta objetoCuenta = new Cuenta("Elias", 36, 1, 10000);
		objetoCuenta.depositar(5000);
		assertEquals(15000, objetoCuenta.verSaldo(), 0.0);

	}

	@Test
	public void pruebaToString() {
		Cuenta objetoCuenta = new Cuenta("Elias", 36, 1, 10000);
		String esperado = "Nombre: Elias Dni: 36 Numero de cuenta: 1 Saldo: 10000";
		String actual = objetoCuenta.toString();
		assertEquals(esperado, actual);

	}

	@Test
	public void pruebaEqual() {
		Cuenta objetoCuentaUno = new Cuenta("Elias", 36, 1, 10000);
		Cuenta objetoCuentaDos = new Cuenta("Elias", 36, 1, 10000);

		assertEquals(true, objetoCuentaDos.equals(objetoCuentaUno));

	}

}
