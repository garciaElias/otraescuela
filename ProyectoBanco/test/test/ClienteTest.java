package test;

import static org.junit.Assert.*;

import org.junit.Test;

import banco.cuentas.Cliente;
import banco.cuentas.CuentaCorriente;
import banco.cuentas.CuentaSueldo;

public class ClienteTest {

	@Test
	public void pruebaAddCuentaCorrienteSolamente() {
		Cliente objetoCliente = new Cliente("Elias", 36);

		CuentaCorriente objUnoCuentaCorriente = new CuentaCorriente("Elias", 36, 1, 10000, 1000);

		assertEquals(true, objetoCliente.add(objUnoCuentaCorriente));
	}

	@Test
	public void pruebaAddCuentaSueldoSolamente() {
		Cliente objetoCliente = new Cliente("Elias", 36);

		CuentaSueldo objCuentaSueldo = new CuentaSueldo("Elias", 36, 1, 10000, "Epidata");

		assertEquals(false, objetoCliente.add(objCuentaSueldo));

	}

	@Test
	public void pruebaAddUnaCuentaSueldoConUnaCuentaCorrienteExistente() {
		Cliente objetoCliente = new Cliente("Elias", 36);

		CuentaSueldo objUnoCuentaSueldo = new CuentaSueldo("Elias", 36, 1, 10000, "Epidata");

		CuentaCorriente objUnoCuentaCorriente = new CuentaCorriente("Elias", 36, 1, 10000, 1000);

		objetoCliente.add(objUnoCuentaCorriente);

		assertEquals(true, objetoCliente.add(objUnoCuentaSueldo));
	}

	@Test
	public void pruebaAddDosCuentaSueldoYDosCuentaCorriente() {
		Cliente objetoCliente = new Cliente("Elias", 36);

		CuentaSueldo objUnoCuentaSueldo = new CuentaSueldo("Elias", 36, 1, 10000, "Epidata");
		CuentaSueldo objDosCuentaSueldo = new CuentaSueldo("Elias", 36, 1, 10000, "UNLaM");

		CuentaCorriente objUnoCuentaCorriente = new CuentaCorriente("Elias", 36, 1, 10000, 1000);
		CuentaCorriente objDosCuentaCorriente = new CuentaCorriente("Elias", 36, 2, 10000, 1000);

		objetoCliente.add(objUnoCuentaCorriente);
		objetoCliente.add(objDosCuentaCorriente);

		objetoCliente.add(objUnoCuentaSueldo);

		assertEquals(true, objetoCliente.add(objDosCuentaSueldo));
	}

	@Test
	public void pruebaAddTresCuentaSueldoYDosCuentaCorriente() {
		Cliente objetoCliente = new Cliente("Elias", 36);

		CuentaSueldo objUnoCuentaSueldo = new CuentaSueldo("Elias", 36, 1, 10000, "Epidata");
		CuentaSueldo objDosCuentaSueldo = new CuentaSueldo("Elias", 36, 2, 10000, "UNLaM");
		CuentaSueldo objTresCuentaSueldo = new CuentaSueldo("Elias", 36, 3, 10000, "Supervielle");

		CuentaCorriente objUnoCuentaCorriente = new CuentaCorriente("Elias", 36, 1, 10000, 1000);
		CuentaCorriente objDosCuentaCorriente = new CuentaCorriente("Elias", 36, 2, 10000, 1000);

		objetoCliente.add(objUnoCuentaCorriente);
		objetoCliente.add(objDosCuentaCorriente);

		objetoCliente.add(objUnoCuentaSueldo);
		objetoCliente.add(objDosCuentaSueldo);

		assertEquals(false, objetoCliente.add(objTresCuentaSueldo));

	}

	@Test
	public void pruebaAddConUnaCuentaSueldoDeOtraPersona() {
		Cliente objetoCliente = new Cliente("Elias", 36166103);

		CuentaSueldo objUnoCuentaSueldo = new CuentaSueldo("Juan", 12345678, 1, 10000, "Sarasa");
		CuentaCorriente objUnoCuentaCorriente = new CuentaCorriente("Elias", 36, 2, 10000, 1000);

		objetoCliente.add(objUnoCuentaCorriente);

		assertEquals(false, objetoCliente.add(objUnoCuentaSueldo));
	}

	@Test
	public void pruebaAddConUnaCuentaSueldoDeLaMismaPersona() {
		Cliente objetoCliente = new Cliente("Elias", 36166103);

		CuentaSueldo objUnoCuentaSueldo = new CuentaSueldo("Elias", 36166103, 1, 10000, "Sarasa");
		CuentaCorriente objUnoCuentaCorriente = new CuentaCorriente("Elias", 36, 2, 10000, 1000);

		objetoCliente.add(objUnoCuentaCorriente);

		assertEquals(true, objetoCliente.add(objUnoCuentaSueldo));
	}

	@Test
	public void pruebaEquals() {

		Cliente objetoClienteUno = new Cliente("Elias", 36);
		Cliente objetoClienteDos = new Cliente("Elias", 36);

		CuentaSueldo objUnoCuentaSueldo = new CuentaSueldo("Elias", 36, 1, 10000, "Epidata");
		CuentaSueldo objDosCuentaSueldo = new CuentaSueldo("Elias", 36, 2, 10000, "UNLaM");
		CuentaSueldo objTresCuentaSueldo = new CuentaSueldo("Elias", 36, 3, 10000, "Supervielle");

		CuentaSueldo objCuatroCuentaSueldo = new CuentaSueldo("Elias", 36, 1, 10000, "Epidata");
		CuentaSueldo objCincoCuentaSueldo = new CuentaSueldo("Elias", 36, 2, 10000, "UNLaM");
		CuentaSueldo objSeisCuentaSueldo = new CuentaSueldo("Elias", 36, 3, 10000, "Supervielle");

		CuentaCorriente objUnoCuentaCorriente = new CuentaCorriente("Elias", 36, 1, 18000, 1000);
		CuentaCorriente objDosCuentaCorriente = new CuentaCorriente("Elias", 36, 2, 10000, 1000);
		CuentaCorriente objTresCuentaCorriente = new CuentaCorriente("Elias", 36, 6, 12000, 1000);

		CuentaCorriente objCuatroCuentaCorriente = new CuentaCorriente("Elias", 36, 1, 18000, 1000);
		CuentaCorriente objCincoCuentaCorriente = new CuentaCorriente("Elias", 36, 2, 10000, 1000);
		CuentaCorriente objSeisCuentaCorriente = new CuentaCorriente("Elias", 36, 6, 12000, 1000);

		// Add 3 cuentas Corriente a CLIENTE UNO
		objetoClienteUno.add(objUnoCuentaCorriente);
		objetoClienteUno.add(objDosCuentaCorriente);
		objetoClienteUno.add(objTresCuentaCorriente);

		// Add 3 cuentas Corriente a CLIENTE DOS
		objetoClienteDos.add(objCuatroCuentaCorriente);
		objetoClienteDos.add(objCincoCuentaCorriente);
		objetoClienteDos.add(objSeisCuentaCorriente);

		// Add 3 cuentas Sueldo a CLIENTE UNO
		objetoClienteUno.add(objUnoCuentaSueldo);
		objetoClienteUno.add(objDosCuentaSueldo);
		objetoClienteUno.add(objTresCuentaSueldo);

		// Add 3 cuentas Sueldo a CLIENTE DOS
		objetoClienteDos.add(objCuatroCuentaSueldo);
		objetoClienteDos.add(objCincoCuentaSueldo);
		objetoClienteDos.add(objSeisCuentaSueldo);

		assertEquals(true, objetoClienteUno.equals(objetoClienteDos));

	}
}
