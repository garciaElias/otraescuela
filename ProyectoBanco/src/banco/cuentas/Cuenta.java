package banco.cuentas;

public class Cuenta {

	private String nombre;
	private int dni;
	private int numeroCuenta;
	private double saldo;

	public Cuenta(String nombre, int dni, int numeroCuenta, double saldo) {
		this.nombre = nombre;
		this.dni = dni;
		this.numeroCuenta = numeroCuenta;
		this.saldo = saldo;
	}

	public String getNombre() {
		return nombre;
	}

	public int getDni() {
		return dni;
	}

	public void depositar(double dinero) {
		this.saldo += dinero;
	}

	public void extraer(double dinero) throws SaldoInsuficienteException {

		this.saldo -= dinero;

	}

	public double verSaldo() {
		return saldo;
	}

	public String toString() {
		return "Nombre: " + nombre + " Dni: " + dni + " Numero de cuenta: " + numeroCuenta + " Saldo: " + (int) saldo;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof Cuenta)) {
			return false;
		}
		return (nombre == ((Cuenta) obj).nombre) && (dni == ((Cuenta) obj).dni)
				&& (numeroCuenta == ((Cuenta) obj).numeroCuenta) && (saldo == ((Cuenta) obj).saldo);
	}

}
