package banco.cuentas;

import java.util.ArrayList;
import java.util.List;

public class Cliente {

	private String nombre;
	private int dni;
	private List<CuentaSueldo> listaCuentaSueldo;
	private List<CuentaCorriente> listaCuentaCorriente;

	/*
	 * set<Cuenta> cuentas= new HashSet<Cuenta>();
	 * 
	 * quizas conviene hacer esto porque como son conjuntos(set), no me va a
	 * dejar agregar repetidos, pero hay que implementar equals y hashCode
	 * 
	 * no conviene usar insteadOf ya que si alguna clase se modifca o se agrega
	 * alguna clase, tendre que modifcar todo el chorizo de cdigo(tendré que
	 * hacer banda de IFs, y es una cagada)
	 */
	/*
	 * De un Error no me puedo recuperar!!son como errores del sist De una
	 * exception si! ya que son errores de logica y no del sistema Exception me
	 * deja hacer algo
	 * 
	 * runtimeException.. equivale a un bug! como objeto null y esas cosas.. o..
	 * no encuentro archivo..el programa informa un error razonable pero em deja
	 * seguir Puedo crear exceptiones para dependiendo del error puedo tiraar un
	 * cartel de alerta
	 *
	 * try el codigo que quiero evaluar catch
	 * 
	 * 
	 * throws adelante de un metodo: me dice que ese metodo puede tener uan
	 * excpetion de tal tipo
	 * 
	 * fail-----> tira una exception en el Test(va a dar en rojo)
	 */
	public Cliente(String nombre, int dni) {
		this.nombre = nombre;
		this.dni = dni;
		listaCuentaSueldo = new ArrayList<CuentaSueldo>();
		listaCuentaCorriente = new ArrayList<CuentaCorriente>();
	}

	public boolean add(CuentaSueldo objetoCuentaSueldo) {
		if (listaCuentaSueldo.size() < listaCuentaCorriente.size() && (nombre == objetoCuentaSueldo.getNombre())
				&& (dni == objetoCuentaSueldo.getDni())) {
			listaCuentaSueldo.add(objetoCuentaSueldo);
			return true;
		} else {
			return false;
		}
	}

	public boolean add(CuentaCorriente objetoCuentaCorriente) {
		return listaCuentaCorriente.add(objetoCuentaCorriente);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof Cliente)) {
			return false;
		}
		// equals en listas usa el equals de los objetos dentro de las listas
		return (nombre == ((Cliente) obj).nombre) && (dni == ((Cliente) obj).dni)
				&& (this.listaCuentaCorriente.equals(((Cliente) obj).listaCuentaCorriente))
				&& (this.listaCuentaSueldo.equals(((Cliente) obj).listaCuentaSueldo));
	}

}
