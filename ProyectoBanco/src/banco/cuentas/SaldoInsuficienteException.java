package banco.cuentas;

public class SaldoInsuficienteException extends Exception {

	public SaldoInsuficienteException() {
		super("Saldo insuficiente");
	}

}
