package banco.cuentas;

public class CuentaCorriente extends Cuenta {

	private final double saldoEnRojo;

	public CuentaCorriente(String nombre, int dni, int numeroCuenta, double saldo, double saldoEnRojo) {
		super(nombre, dni, numeroCuenta, saldo);
		this.saldoEnRojo = saldoEnRojo;
	}

	public void extraer(double dinero) throws SaldoInsuficienteException {
		if ((super.verSaldo() - dinero) >= (-saldoEnRojo)) {
			super.extraer(dinero);
		} else {
			throw new SaldoInsuficienteException();
		}
	}

	public double verSaldoEnRojo() {
		if (super.verSaldo() < 0)
			return -super.verSaldo();
		else
			return 0;
	}

	public String toString() {
		return super.toString() + " Saldo en rojo: " + (int) saldoEnRojo;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof CuentaCorriente)) {
			return false;
		}

		return super.equals((Cuenta) obj) && saldoEnRojo == ((CuentaCorriente) obj).saldoEnRojo;
	}

}
