package banco.cuentas;

public class CuentaSueldo extends Cuenta {

	private String empleador;

	public CuentaSueldo(String nombre, int dni, int numeroCuenta, double saldo, String empleador) {
		super(nombre, dni, numeroCuenta, saldo);
		this.empleador = empleador;
	}

	public String toString() {
		return super.toString() + " Empleador: " + empleador;
	}

	public void extraer(double dinero) throws SaldoInsuficienteException {

		if (this.verSaldo() >= dinero) {

			super.extraer(dinero);
		} else {
			throw new SaldoInsuficienteException();
		}

	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof CuentaSueldo)) {
			return false;
		}

		return super.equals((Cuenta) obj) && (empleador == ((CuentaSueldo) obj).empleador);
	}

}
