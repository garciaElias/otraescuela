var app = angular.module("app", []);
 
app.controller("TablaCCorrienteController", function($scope) {
  $scope.cuentasCorrientes=[
    {
      nroCuentaCorriente:2789798778789557,
      saldo:"78229.12"
    },
    {
      nroCuentaCorriente:8728919789738215,
      saldo:"6984.54"
    },
    {
      nroCuentaCorriente:22798973838957891,
      saldo:"24846.26"
	  },
    {
      nroCuentaCorriente:97201368297922969,
      saldo:"24678.56"
    },  
    {
      nroCuentaCorriente:36829798927997926,
      saldo:"24854.27"
    }
  ];
});

app.controller("TablaCSueldoController", function($scope) {
  $scope.cuentasSueldos=[
    {
      nroCuentaSueldo:2789798778179871,
	  empleador:"YPF",
      saldo:"78229.12"
    },
    {
      nroCuentaSueldo:7898781978973215,
	  empleador:"Movistar",
      saldo:"6984.54"
    },
    {
      nroCuentaSueldo:2566791387157891,
	  empleador:"Epidata",
      saldo:"24846.26"
	  },
    {
      nroCuentaSueldo:4577282717892798,	
	  empleador:"UNLaM",
      saldo:"24678.56"
    },  
    {
      nroCuentaSueldo:2787178268791786,
	  empleador:"Petrobras",
      saldo:"24854.27"
    }
  ];
});